/******************************************************************************
 *
 *  Copyright (C) 2021 Bjarne von Horn, Ingenieurgemeinschaft IgH
 *
 *  This file is part of the IgH EtherCAT example application collection.
 *
 *  The IgH EtherCAT example application collection is free software; you can
 *  redistribute it and/or modify it under the terms of the GNU Lesser General
 *  Public License as published by the Free Software Foundation; version 2.1
 *  of the License.
 *
 *  The IgH EtherCAT example application collection is distributed in the hope that
 *  it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the IgH EtherCAT example application collection. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 *****************************************************************************/

#include "blinker.h"
#include <unistd.h>
#include "pdserv_wrapper.h"
#include <time.h>
#include <mutex>

int main()
{
    Blinker t;
    pdservWrapper serv{"test", "1.2.3", +[](timespec *time) { return clock_gettime(CLOCK_REALTIME, time); }};
    std::mutex mutex;

    const double tsample = 0.01;
    auto task = serv.create_task(tsample, "Task1");
    task.set_signal_writelock(mutex);
    task.signal(1, "/input/klemme1", t.getInputPtr(), 1);

    serv.prepare();
    t.switch_rt();

    for (;;)
    {
        timespec world_time;
        clock_gettime(CLOCK_REALTIME, &world_time);
        {
            std::unique_lock<std::mutex> lck(mutex);
            t.execute();
        }
        task.update(world_time);
        usleep(100);
    }
}
