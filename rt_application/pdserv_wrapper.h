#pragma once

#include "wrapper_base.h"

#include <pdserv.h>
#include <stdexcept>
#include "pdserv_traits.h"
#include <mutex>

struct pdTaskWrapper
{
    pdtask *task_;

    template <typename T>
    auto signal(unsigned int decimation,
                const char *path, T const *addr, size_t n, const size_t *dim = nullptr)
    {
        constexpr auto dtype = pdservTypeTraits<T>::identifier;
        return pdserv_signal(task_, decimation, path, dtype, addr, n, dim);
    }

    void update(const timespec &t = {0, 0})
    {
        pdserv_update(task_, &t);
    }

    template <typename T>
    void set_signal_writelock_cb(T &cb)
    {
        pdserv_set_signal_readlock_cb(
            task_, +[](int lock, void *p) {
                T &restored_cb = *reinterpret_cast<T *>(p);
                restored_cb(lock);
            },
            const_cast<void *>(static_cast<const void *>(&cb)));
    }

    void set_signal_writelock(std::mutex &mut)
    {
        pdserv_set_signal_readlock_cb(
            task_, [](int lock, void *m) {
                std::mutex &mut(*reinterpret_cast<std::mutex *>(m));
                if (lock)
                    mut.lock();
                else
                    mut.unlock();
            },
            &mut);
    }
};

struct pdservWrapper : WrapperBase<pdserv, pdserv_exit>
{
    pdservWrapper(const char *name, const char *version, gettime_t gettime_cb)
        : WrapperBase(pdserv_create(name, version, gettime_cb), WrapperBase::nullcheck)
    {
    }

    void prepare()
    {
        if (pdserv_prepare(get()) != 0)
            throw std::runtime_error("could not start pdserv");
    }

    pdTaskWrapper create_task(double tsample, const char *name)
    {
        auto ans = pdserv_create_task(get(), tsample, name);
        if (!ans)
            throw std::runtime_error("could not create task");
        return {ans};
    }
};
