#pragma once

#include <utility>
#include <stdexcept>

template <typename T, void (*deleter)(T *)>
class WrapperBase
{
    T *object_;

public:
    struct nullcheck_t
    {
    };
    static constexpr nullcheck_t nullcheck{};

    explicit WrapperBase(T *obj) noexcept : object_(obj) {}
    WrapperBase(T *obj, nullcheck_t) : object_(obj)
    {
        if (!object_)
            throw std::runtime_error("could not create pdserv instance");
    }

    WrapperBase(WrapperBase const &) = delete;
    WrapperBase(WrapperBase &&other) noexcept : object_(nullptr)
    {
        other.swap(*this);
    }
    WrapperBase &operator=(WrapperBase const &) = delete;
    WrapperBase &operator=(WrapperBase &&other) noexcept
    {
        WrapperBase copy(std::move(other));
        copy.swap(*this);
        return *this;
    }

    void swap(WrapperBase &other) noexcept
    {
        T *tmp = object_;
        object_ = other.object_;
        other.object_ = tmp;
    }

    T *get() const noexcept
    {
        return object_;
    }

    T *reset(T *other = nullptr) noexcept
    {
        T *result = object_;
        object_ = other;
        return result;
    }

protected:
    ~WrapperBase()
    {
        if (object_)
            deleter(object_);
    }
};
