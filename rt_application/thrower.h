#pragma once

#include <stdexcept>


struct Thrower
{
    Thrower() = default;
    explicit Thrower(int i)
    {
        if (i)
            throw std::runtime_error("Code returned 0");
    }

    void operator=(int i)
    {
        if (i)
            throw std::runtime_error("Code returned 0");
    }

    explicit Thrower(const void *p)
    {
        if (!p)
            throw std::runtime_error("resource allocation failed");
    }

    void operator=(const void *p)
    {
        if (!p)
            throw std::runtime_error("resource allocation failed");
    }
};
