pkg_check_modules(ethercat REQUIRED IMPORTED_TARGET libethercat libpdserv)

add_executable(rt_application
    blinker.cpp
    main.cpp
)

target_link_libraries(rt_application PUBLIC PkgConfig::ethercat)
