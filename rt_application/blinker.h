#pragma once

#include <ecrt.h>
#include "ec_wrapper.h"

/* Master 0, Slave 1, "EL2024"
 * Vendor ID:       0x00000002
 * Product code:    0x07e83052
 * Revision number: 0x00100000
 */

/* Master 0, Slave 3, "EL1014"
 * Vendor ID:       0x00000002
 * Product code:    0x03f63052
 * Revision number: 0x00000000
 */

#define BUS_COUPLER_POS 0, 0
#define OUTPUT_SLAVE_POS 0, 1
#define DIG_INPUT_SLAVE_POS 0, 3
#define EL1014 0x00000002, 0x03f63052
#define EL2024 0x00000002, 0x07e83052
#define EK1100 0x00000002, 0x044c2c52

class Blinker
{
    ECMaster master_{0};
    ec_domain_t *domain_ = nullptr;
    unsigned int slave1_offset_ = 0, slave3_offset_ = 0;
    uint8_t *pdata_ = nullptr;
    bool blink_ = false;
    int counter_ = 500;

public:
    Blinker();

    auto getInputPtr()
    {
        return const_cast<const uint8_t *>(pdata_) + slave3_offset_;
    }

    void switch_rt();

    void execute() noexcept;
};
