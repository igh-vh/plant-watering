#include "thrower.h"
#include "blinker.h"

#include <iostream>

void Blinker::switch_rt()
{
    //mlockall
    //setscheduler
}

void Blinker::execute() noexcept
{
    master_.recieve();
    ecrt_domain_process(domain_);

    if (!counter_--)
    {
        blink_ = !blink_;
        EC_WRITE_U8(pdata_ + slave1_offset_, blink_);
        counter_ = 500;
        std::cout << "\rIN is " << static_cast<int>(*(getInputPtr())) << std::flush;
    }

    ecrt_domain_queue(domain_);
    master_.send();
}

Blinker::Blinker()
{
    Thrower ret;
    ec_slave_config_t *sc;
    domain_ = master_.create_domain();

    // setup bus coupler
    ret = sc = ecrt_master_slave_config(master_.get(), BUS_COUPLER_POS, EK1100);

    // setup output
    ec_pdo_entry_info_t slave_1_pdo_entries[] = {
        {0x7000, 0x01, 1}, /* Output */
        {0x7010, 0x01, 1}, /* Output */
        {0x7020, 0x01, 1}, /* Output */
        {0x7030, 0x01, 1}, /* Output */
    };

    ec_pdo_info_t slave_1_pdos[] = {
        {0x1600, 1, slave_1_pdo_entries + 0}, /* Channel 1 */
        {0x1601, 1, slave_1_pdo_entries + 1}, /* Channel 2 */
        {0x1602, 1, slave_1_pdo_entries + 2}, /* Channel 3 */
        {0x1603, 1, slave_1_pdo_entries + 3}, /* Channel 4 */
    };

    ec_sync_info_t slave_1_syncs[] = {
        {0, EC_DIR_OUTPUT, 4, slave_1_pdos + 0, EC_WD_ENABLE},
        {0xff},
    };

    ret = sc = ecrt_master_slave_config(master_.get(), OUTPUT_SLAVE_POS, EL2024);

    ret = ecrt_slave_config_pdos(sc, EC_END, slave_1_syncs);

    // setup digital input
    ec_pdo_entry_info_t slave_3_pdo_entries[] = {
        {0x3101, 0x01, 1}, /* Input */
        {0x3101, 0x02, 1}, /* Input */
        {0x3101, 0x03, 1}, /* Input */
        {0x3101, 0x04, 1}, /* Input */
    };

    ec_pdo_info_t slave_3_pdos[] = {
        {0x1a00, 1, slave_3_pdo_entries + 0}, /* Channel 1 */
        {0x1a01, 1, slave_3_pdo_entries + 1}, /* Channel 2 */
        {0x1a02, 1, slave_3_pdo_entries + 2}, /* Channel 3 */
        {0x1a03, 1, slave_3_pdo_entries + 3}, /* Channel 4 */
    };

    ec_sync_info_t slave_3_syncs[] = {
        {0, EC_DIR_INPUT, 4, slave_3_pdos + 0, EC_WD_DISABLE},
        {0xff},
    };

    ret = sc = ecrt_master_slave_config(master_.get(), DIG_INPUT_SLAVE_POS, EL1014);
    ret = ecrt_slave_config_pdos(sc, EC_END, slave_3_syncs);

    // finally setup pdo mapping
    const ec_pdo_entry_reg_t domain1_regs[] =
        {
            {OUTPUT_SLAVE_POS, EL2024, 0x7000, 1, &slave1_offset_},
            {DIG_INPUT_SLAVE_POS, EL1014, 0x3101, 1, &slave3_offset_},
            {},
        };

    ret = ecrt_domain_reg_pdo_entry_list(domain_, domain1_regs);

    ret = ecrt_master_activate(master_.get());
    ret = pdata_ = ecrt_domain_data(domain_);
}
