#pragma once

#include <pdserv.h>

template <typename T>
struct pdservTypeTraits
{
};

template <>
struct pdservTypeTraits<double>
{
    static constexpr int identifier = pd_double_T;
};

template <>
struct pdservTypeTraits<unsigned char>
{
    static constexpr int identifier = pd_uchar_T;
};
