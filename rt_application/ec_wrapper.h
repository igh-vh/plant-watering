#pragma once

#include "wrapper_base.h"
#include <ecrt.h>

struct ECMaster : WrapperBase<ec_master_t, ecrt_release_master>
{
    explicit ECMaster(unsigned int master_index) : WrapperBase(ecrt_request_master(master_index), WrapperBase::nullcheck)
    {
    }

    void recieve()
    {
        ecrt_master_receive(get());
    }

    void send()
    {
        ecrt_master_send(get());
    }

    ec_domain_t * create_domain()
    {
        auto ans = ecrt_master_create_domain(get());
        if (!ans)
            throw std::runtime_error("could not create domain");
        return ans;
    }
};
