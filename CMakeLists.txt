cmake_minimum_required(VERSION 3.10)

project(plant_watering)

find_package(PkgConfig REQUIRED)

add_subdirectory(rt_application)
add_subdirectory(cli_client)
