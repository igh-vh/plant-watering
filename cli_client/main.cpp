/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2009  Richard Hacker <lerich@gmx.net>
 *                     Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 * \file PdCom example implementation.
 *
 ****************************************************************************/

#include <iostream>

#include "CliProcess.h"

using std::cerr;
using std::endl;

/****************************************************************************/

int main(int argc, const char *argv[])
{
    int retval = 0;

    try {
        CliProcess process("localhost", 2345);
        process.exec();
    } catch (LinuxProcess::Exception &e) {
        cerr << "LinuxProcess::Exception: " << e.what() << endl;
        retval = 1;
    } catch (PdCom::Exception &e) {
        cerr << "PdCom::Exception: " << e.what() << endl;
        retval = 1;
    }

    return retval;
}

/****************************************************************************/
