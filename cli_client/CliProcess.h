#pragma once

#include "LinuxProcess.h"

class CliProcess : public LinuxProcess, public PdCom::Subscriber
{
public:
    CliProcess(const char *address,
               int port) : LinuxProcess(address, port), scale()
    {
    }

    ~CliProcess()
    {
        // do not omit, because it  calls notifyDelete
        // which is a virtual function
        reset();
    }

    // from PdCom::Process
    void sigConnected();

    // from PdCom::Subscriber
    void notify(PdCom::Variable *);
    void notifyDelete(PdCom::Variable *);

private:
    PdCom::Variable::Scale scale;
};
