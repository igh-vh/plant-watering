/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2009  Richard Hacker <lerich@gmx.net>
 *                     Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 * \file PdCom example implementation.
 *
 ****************************************************************************/

#include <string>
#include <stdexcept>

#include "pdcom.h"

/****************************************************************************/

/* PdCom::Process implementation for Linux.
 *
 * Uses a TCP socket to connect to the process.
 */
class LinuxProcess : public PdCom::Process
{
public:
    LinuxProcess(const char * path, int port, bool blocking = false);
    ~LinuxProcess();

    void exec(void);

    /** Exception class.
         */
    class Exception : public std::runtime_error
    {
    public:
        /** Constructor. */
        Exception(const std::string &s /**< Message */) : std::runtime_error(s){};
    };

protected:
    bool finished;

private:
    int fd;
    bool writeRequest;

    int readSocket();
    int writeSocket();

    // PdCom::Process
    void sendRequest() override;
    int sendData(const char *, size_t) override;
    bool clientInteraction(const std::string &, const std::string &,
                           const std::string &, std::list<ClientInteraction> &) override;
};

/****************************************************************************/
