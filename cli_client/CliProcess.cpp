#include "CliProcess.h"

#include <iostream>

void CliProcess::sigConnected()
{
    PdCom::Variable *const pv = findVariable("/input/klemme1");
    if (!pv)
    {
        std::cerr << "Could not find variable!\n";
        finished = true;
        return;
    }
    try
    {
        pv->subscribe(this, 0.2);
    }
    catch (PdCom::Exception &e)
    {
        std::cerr << "An error occurred while trying to subscribe "
                     "to a variable: "
                  << e.what() << std::endl;
        finished = true;
    }
}

void CliProcess::notify(PdCom::Variable *var)
{
    unsigned char data;
    if (var->type != PdCom::Data::uint8_T)
    {
        std::cerr << "Variable has wrong data type\n";
        return;
    }
    if (var->dimension.getElementCount() != 1)
    {
        std::cerr << "Wrong number of elements: " << var->dimension.getElementCount() << "\n";
        return;
    }
    var->getValue(&data, 1);
    std::cout << "\r[IN] " << static_cast<unsigned int>(data) << std::flush;
}

void CliProcess::notifyDelete(PdCom::Variable *var)
{
}
